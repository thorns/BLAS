#! /bin/bash

################################################################################
# Prepare
################################################################################

# Set up shell
if [ "$(echo ${VERBOSE} | tr '[:upper:]' '[:lower:]')" = 'yes' ]; then
    set -x                      # Output commands
fi
set -e                          # Abort on errors
################################################################################
# Configure Cactus
################################################################################

pkg-config --exists blas || exit 1
BLAS_INC_DIRS=$(pkg-config --cflags-only-I blas | sed 's/-I//g')
BLAS_LIB_DIRS=$(pkg-config --libs-only-L blas | sed 's/-L//g')
BLAS_LIBS=$(pkg-config --libs-only-l blas | sed 's/-l//g')

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "HAVE_BLAS     = 1"
echo "BLAS_DIR      = ${BLAS_DIR}"
echo "BLAS_INC_DIRS = ${BLAS_INC_DIRS}"
echo "BLAS_LIB_DIRS = ${BLAS_LIB_DIRS}"
echo "BLAS_LIBS     = ${BLAS_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(BLAS_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(BLAS_LIB_DIRS)'
echo 'LIBRARY           $(BLAS_LIBS)'
